srctree := .
export srctree
_PHONY := all

MAKEFLAGS += --no-print-directory
INCLUDES := 
# Set target name
-include .config
ifeq ($(target), )
ifneq ($(findstring _defconfig,$(MAKECMDGOALS)),_defconfig)
$(error "No configuration setup, run make x_defconfig to setup")
endif
endif


# Set-up toolchain
TOOLCHAIN := arm-none-eabi-
AR        := $(TOOLCHAIN)ar
AS        := $(TOOLCHAIN)as
CC        := $(TOOLCHAIN)gcc
LD        := $(TOOLCHAIN)gcc
OBJDUMP   := $(TOOLCHAIN)objdump
OBJCOPY   := $(TOOLCHAIN)objcopy
SIZE      := $(TOOLCHAIN)size

# Set default CFLAGS
CFLAGS     = $(CPU)
CFLAGS    += -g -O0
#CFLAGS    += -DSTM32F746xx
#CFLAGS    += -ffunction-sections -fdata-sections
# Set default AFLAGS
AFLAGS     = $(CPU)

# Export toolchain and default flags
export AR AS CC LD OBJDUMP OBJCOPY SIZE TOOLCHAIN AFLAGS CFLAGS

# Linker flags
LDFLAGS   := -specs=nano.specs -specs=nosys.specs
LDFLAGS   += -Wl,-Map=$(target).map
#LDFLAGS   += -Wl,--gc-sections
LDFLAGS   += -Wl,-static

# Include common includes
include scripts/Makefile.include

# Set-up global project include search paths
INCLUDES += -I $(CMSIS)
INCLUDES += -I $(CMSIS_DEVICE)
INCLUDES += -I $(STHAL)



# Export global includes
export INCLUDES

# Set up build directories
dir-y := $(CUBE)/ $(APPLICATION)/ drivers/ graphics/


_PHONY += $(dirs)
dirs := $(patsubst %/,%, $(dir-y))

# Add built-in target to build directories
_PHONY += $(dir-y)
dir-y := $(patsubst %/,%/built-in.o,$(dir-y))

# Set-up directories to clean
clean-dirs := $(addprefix _clean_, $(dirs))


_PHONY += $(dirs)
all: link 

$(dir-y): $(dirs)

link: $(dir-y)
	$(Q)$(MAKE) $(target) $(link)=$(target)


clean: $(clean-dirs)
	@rm -f $(tgt)

$(clean-dirs):
	$(Q)$(MAKE) $(clean)=$(patsubst _clean_%,%,$@)

$(dirs):
	$(Q)$(MAKE) $(build)=$@
_PHONY += %_defconfig
%_defconfig: config/.%_config
	@echo "Setting up configuration for $*, $@"
	@cp $< ./.config

ifneq ($(skip-phony),y)
.PHONY : $(_PHONY)
endif
