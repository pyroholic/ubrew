#include "drivers/ds18b20.h"
#include "drivers/onewire.h"

#define DS18B20_RESOLUTION_MASK 0x60

int ds18b20_setResolution(int ow, ds18b20_res_t resolution) {
   /* Buffer for scratchpad data */
   uint8_t scratchpad[3];
   int i;
   /* Send reset pulse */
   if (onewire_reset(ow)) {
      return E_NOT_PRESENT;
   }
   /* Write Skip ROM command */
   onewire_writeByte(ow, skip_rom);
   onewire_writeByte(ow, read_scratchpad);
   /* Ignore measurement data */
   onewire_readByte(ow);
   onewire_readByte(ow);
   for(i=0;i<3;i++) {
      scratchpad[i] = onewire_readByte(ow);
   }
   /* Send reset pulse to abort transaction */
   if(onewire_reset(ow)) {
      return E_NOT_PRESENT;
   }
   scratchpad[2] &= ~DS18B20_RESOLUTION_MASK;
   scratchpad[2] |= (resolution & DS18B20_RESOLUTION_MASK);
   onewire_writeByte(ow, skip_rom);
   onewire_writeByte(ow, write_scratchpad);
   for(i=0;i<3;i++) {
      onewire_writeByte(ow, scratchpad[i]);
   }
   return 0;
}

int ds18b20_readTemp(int ow) {
   int i;
   int16_t temp;
   uint8_t scratchpad[9];
   uint8_t crc;
   if (onewire_reset(ow)) {
      return E_NOT_PRESENT;
   }
   onewire_writeByte(ow, skip_rom);
   onewire_writeByte(ow, read_scratchpad);
   //onewire_reset(ow);
   for(i=0;i<9;i++) {
      scratchpad[i] = onewire_readByte(ow);
   }
   crc = onewire_crc8(scratchpad, 8);
   if (crc != scratchpad[8]) {
      return E_CRC_MISMATCH;
   }
   temp = scratchpad[1] << 8 | scratchpad[0];
   return (int)temp;
}

int ds18b20_startConversion(int ow) {
   if (onewire_reset(ow)) {
      return E_NOT_PRESENT;
   }
   /* Send reset pulse */
   onewire_writeByte(ow, skip_rom);
   /* Send conversion command */
   onewire_writeByte(ow, 0x44);
   return 0;
}
