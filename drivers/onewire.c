#include "drivers/onewire.h"

#include "gpio.h"

#include "delay.h"
#include "device.h"
#ifdef STM32F4xx
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_gpio.h"
#include "../fridge/include/stm32f4xx.h"
#else
#include "stm32f7xx_hal_rcc.h"
#include "stm32f7xx_hal_gpio.h"
#include "stm32f7xx.h"
#endif

#include <stdint.h>

/* Private type definitions */
typedef enum {
   uninitialized,
   initialized,
} OneWire_state_t;

typedef struct {
   GPIO_TypeDef *  GPIOx;
   uint16_t        pin;
   uint8_t         serial[8];
   OneWire_state_t state;
} OneWire_t;

/* Declarations of local data */
static int onewire_slots[4] = {-1, -1, -1, -1};
static OneWire_t onewire[4];

static const uint8_t crc_table[] = {
      0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
      157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
      35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
      190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
      70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
      219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
      101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
      248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
      140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
      17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
      175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
      50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
      202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
      87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
      233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
      116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53};

/* Definitions */
#define GPIO_REG_SIZE (GPIOB_BASE - GPIOA_BASE)
#define GPIO_REG_OFFSET(x) ((uint32_t)x-GPIOA_BASE)

/* Private functions */

/**
 * @brief Write one bit
 *        Writes one bit to the OneWire bus
 * @param ow: OneWire instance50
 *
 * This function writes one bit to the OneWire bus. The micro controller pulls
 * the GPIO pin assigned to the OneWire instance 'low' to start the transaction,
 * followed by either releasing the pin or keeping it pulled low to write the data.
 * The GPIO pin needs to be pulled low 1-15us before releasing to write a '1'.
 */

static void onewire_writeBit(int ow, uint8_t bit) {
   /* Pull pin low */
   GPIO_setPinLow(onewire[ow].GPIOx, onewire[ow].pin);
   GPIO_setDirection(onewire[ow].GPIOx, onewire[ow].pin, GPIO_MODE_OUTPUT_OD);
   /* Wait for 10us to start write slot */
   delay_us(2);
   /* Write bit to pin */
   if (bit) {
      GPIO_setPinHigh(onewire[ow].GPIOx, onewire[ow].pin);
   }
   /* wait for 55us to complete transaction */
   delay_us(58);
   /* Release pin */
   GPIO_setPinHigh(onewire[ow].GPIOx, onewire[ow].pin);
   GPIO_setDirection(onewire[ow].GPIOx, onewire[ow].pin, GPIO_MODE_INPUT);
}

/**
 *  @brief Read one bit
 *         Reads one bit from the OneWire bus
 *  @param ow: OneWire instance
 *
 *  This function reads one bit from the OneWire bus
 *  The micro controller pulls the GPIO pin assigned to the OneWire instance,
 *  releases the pin and reads the data. The OneWire bus needs to be pulled
 *  down minimum 1us. The read data is valid for 15us from the start of the
 *  transaction. Total transaction time is 60-120us.
 */

static uint8_t onewire_readBit(int ow) {
   uint8_t pinState;
   /* Pull pin low */
   GPIO_setPinLow(onewire[ow].GPIOx, onewire[ow].pin);
   GPIO_setDirection(onewire[ow].GPIOx, onewire[ow].pin, GPIO_MODE_OUTPUT_OD);
   /* Wait for 2us to start read slot*/
   delay_us(2);
   /* Release pin */
   GPIO_setPinHigh(onewire[ow].GPIOx, onewire[ow].pin);
   GPIO_setDirection(onewire[ow].GPIOx, onewire[ow].pin, GPIO_MODE_INPUT);
   /* Wait 10us for stable value */
   delay_us(10);
   pinState = GPIO_readPin(onewire[ow].GPIOx, onewire[ow].pin);
   /* Wait for read slot to expire */
   delay_us(48);
   /* Transaction finished after 60us */
   return pinState;
}

void onewire_writeByte(int ow, uint8_t byte) {
   int i;
   for(i=0;i<8;i++) {
      onewire_writeBit(ow, byte&0x1);
      byte >>= 1;
   }
}

uint8_t onewire_readByte(int ow) {
   int i;
   uint8_t byte = 0;
   for(i=0;i<8;i++) {
      byte >>= 1;
      byte |= (onewire_readBit(ow) << 7);
   }
   return byte;
}

/* Public functions */

/**
 * @breif Initializes a OneWire instance
 *
 * @param GPIOx: GPIO port where OneWire devices are connected
 * @param pin: GPIO pin where OneWire devices are connected
 *
 * Initializes a OneWire instance, assigns the GPIO port and Pin
 * to the instance and returns instance number for use in application
 * code.
 */

int onewire_init(GPIO_TypeDef * GPIOx, uint16_t pin) {
   int i;
   int ret = -1;
   uint32_t gpio_module;
   GPIO_InitTypeDef  GPIO_InitStruct;
   /* Find empty onewire slot */
   for(i=0;i<4;i++) {
      if (onewire_slots[i] == -1) {
         /* Reserve slot */
         onewire_slots[i] = 1;
         /* Enable GPIO clock */
         gpio_module = GPIO_REG_OFFSET(GPIOx) / GPIO_REG_SIZE;
         RCC->AHB1ENR |= (1u << gpio_module);
         /* Initialize GPIO pin */
         GPIO_InitStruct.Pin = pin;
         GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
         GPIO_InitStruct.Pull = GPIO_NOPULL;
         GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
         GPIO_InitStruct.Alternate = 0;
         HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
         /* Update OneWire slot */
         onewire[i].GPIOx = GPIOx;
         onewire[i].pin = pin;
         onewire[i].state = uninitialized;
         ret = i;
         break;
      }
   }
   return ret;
}

/**
 * @breif Resets OneWire instance.
 * @param ow: OneWire instance
 *
 * Resets a OneWire instance by sending a reset pulse.
 * @retval Returns 0 if a presence pulse is detected, otherwise -1 to indicate fault.
 */

int onewire_reset(int ow) {
   int ret = -1;
   GPIO_PinState state;
   /* Pull pin down and set direction to output */
   GPIO_setPinLow(onewire[ow].GPIOx, onewire[ow].pin);
   GPIO_setDirection(onewire[ow].GPIOx, onewire[ow].pin, GPIO_MODE_OUTPUT_OD);
   /* Wait for 480us */
   delay_us(480);
   /* Release pin and set direction to input */
   GPIO_setPinHigh(onewire[ow].GPIOx, onewire[ow].pin);
   GPIO_setDirection(onewire[ow].GPIOx, onewire[ow].pin, GPIO_MODE_INPUT);
   /* Wait for 70us and read pin state (presence pulse) */
   delay_us(80);
   state = GPIO_readPin(onewire[ow].GPIOx, onewire[ow].pin);
   /* Presence pulse detected */
   if (state == GPIO_PIN_RESET) {
      onewire[ow].state = initialized;
      /* Wait for reset to finish */
      delay_us(400);
      /* Return SUCCESS */
      ret = 0;
   }
   return ret;
}

uint8_t onewire_crc8(uint8_t * data, int size) {
   uint8_t crc = 0;
   while (size--) {
      crc = crc_table[crc ^ *data];
      data++;
   }
   return crc;
}
