#include "device.h"
#ifdef STM32F4xx
#include "stm32f4xx_hal_gpio.h"
#else
#include "stm32f7xx_hal_gpio.h"
#endif
int onewire_init(GPIO_TypeDef * GPIOp, uint16_t pin);
int onewire_reset(int ow);
uint8_t onewire_crc8(uint8_t * data, int size);
void onewire_writeByte(int ow, uint8_t byte);

uint8_t onewire_readByte(int ow);

typedef enum {
   search_rom = 0xf0,
   read_rom = 0x33,
   match_rom = 0x55,
   skip_rom = 0xcc,
   read_scratchpad = 0xbe,
   write_scratchpad = 0x4e,
   copy_scratchpad = 0x48

} OneWire_cmd_t;
