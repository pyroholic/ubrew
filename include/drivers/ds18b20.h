#define E_UNKNOWN     -1
#define E_NOT_PRESENT 0x80000000
#define E_CRC_MISMATCH 0x80000001

typedef enum {
   resolution_9bits = 0x0,   /*!< DS18B20 9 bits resolution */
   resolution_10bits = 0x20, /*!< DS18B20 10 bits resolution */
   resolution_11bits = 0x40, /*!< DS18B20 11 bits resolution */
   resolution_12bits = 0x60  /*!< DS18B20 12 bits resolution */
} ds18b20_res_t;

int ds18b20_readTemp(int ow);
int ds18b20_setResolution(int ow, ds18b20_res_t resolution);
int ds18b20_startConversion(int ow);
