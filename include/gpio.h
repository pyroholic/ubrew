#include "device.h"

#ifdef STM32F4xx
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal.h"
#else
#include "stm32f7xx_hal_gpio.h"
#include "stm32f7xx_hal.h"
#endif

/* Defines from stm32f7xx_hal_gpio.c */
#define GPIO_MODE             ((uint32_t)0x00000003U)
#define GPIO_NUMBER           ((uint32_t)16U)

/* Function prototypes */
void GPIO_setDirection(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin, uint32_t mode);

/* Inlined functions */
static inline void GPIO_setPinLow(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin) {
   /* Write to reset register */
   GPIOx->BSRR = (GPIO_Pin << 16);
}

static inline void GPIO_setPinHigh(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin) {
   /* Write to set register */
   GPIOx->BSRR = GPIO_Pin;
}

static inline int GPIO_readPin(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin) {
   /* Read pin state */
   return ((GPIOx->IDR & GPIO_Pin) != 0);
}
