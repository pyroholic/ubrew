#include "delay.h"
#include "stm32f746xx.h"
#include "stm32f7xx_hal_rcc.h"

static uint32_t usTicks;
void delay_init(void) {
   CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
   DWT->LAR = 0xC5ACCE55;
   DWT->CYCCNT = 0;
   DWT->CTRL = DWT_CTRL_CYCCNTENA_Msk;
   usTicks = HAL_RCC_GetHCLKFreq() / 1000000;
}

void delay_us(uint32_t us) {
   uint32_t ticks = us * usTicks;
   uint32_t time = DWT->CYCCNT;
   while ((DWT->CYCCNT - time) < ticks);
}
