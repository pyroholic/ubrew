#include "stm32f7xx_hal.h"
#include <string.h>

static TIM_HandleTypeDef timHandle;
static TIM_OC_InitTypeDef tim_oc;

static TIM_HandleTypeDef rims_timHandle;
static TIM_OC_InitTypeDef rims_tim_oc;

void pwm_init(void) {
   HAL_StatusTypeDef status;
   uint32_t uwPrescalerValue = (uint32_t)((SystemCoreClock / 2) / 10000) - 1;
   /*TIM_HandleTypeDef timHandle;
   TIM_OC_InitTypeDef tim_oc;*/
   memset(&timHandle, 0, sizeof(timHandle));
   memset(&tim_oc, 0, sizeof(tim_oc));
   timHandle.Instance = TIM3;
   timHandle.Init.Prescaler = uwPrescalerValue;
   timHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
   timHandle.Init.Period = 10000-1;
   timHandle.Init.ClockDivision = 0;
   __GPIOB_CLK_ENABLE();
   __TIM3_CLK_ENABLE();


   GPIO_InitTypeDef gpio;
   gpio.Pin = GPIO_PIN_4;
   gpio.Alternate = GPIO_AF2_TIM3;
   gpio.Mode = GPIO_MODE_AF_PP;
   gpio.Pull = GPIO_NOPULL;
   gpio.Speed = GPIO_SPEED_FAST;
   HAL_GPIO_Init(GPIOB, &gpio);

   /*status = HAL_TIM_Base_Init(&timHandle);
   status = HAL_TIM_Base_Start(&timHandle);*/


   tim_oc.Pulse = 7500-1;
   tim_oc.OCMode = TIM_OCMODE_PWM2;
   tim_oc.OCFastMode = TIM_OCFAST_ENABLE;
   tim_oc.OCIdleState = TIM_OCMODE_ACTIVE;
   tim_oc.OCPolarity = TIM_OCPOLARITY_LOW;
   /*HAL_TIM_OC_Init(&timHandle);
   HAL_TIM_OC_ConfigChannel(&timHandle, &tim_oc, TIM_CHANNEL_1);
   HAL_TIM_OC_Start(&timHandle, TIM_CHANNEL_1);*/

   HAL_TIM_PWM_Init(&timHandle);
   HAL_TIM_PWM_ConfigChannel(&timHandle, &tim_oc, TIM_CHANNEL_1);
   HAL_TIM_PWM_Start(&timHandle, TIM_CHANNEL_1);

}

void pwm_set(float duty) {
   /*tim_oc.Pulse = 100*duty - 1;
   HAL_TIM_PWM_ConfigChannel(&timHandle, &tim_oc, TIM_CHANNEL_1);
   HAL_TIM_PWM_Start(&timHandle, TIM_CHANNEL_1);*/
   TIM3->CCR1 = 100*duty-1;
}

void rims_pwm_init(void) {
   // RIMS PWM, D6, GPIOH6
   HAL_StatusTypeDef status;
   uint32_t uwPrescalerValue = (uint32_t)((SystemCoreClock / 2) / 10000) - 1;
   /*TIM_HandleTypeDef timHandle;
   TIM_OC_InitTypeDef tim_oc;*/
   memset(&rims_timHandle, 0, sizeof(rims_timHandle));
   memset(&rims_tim_oc, 0, sizeof(rims_tim_oc));
   rims_timHandle.Instance = TIM12;
   rims_timHandle.Init.Prescaler = uwPrescalerValue;
   rims_timHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
   rims_timHandle.Init.Period = 10000-1;
   rims_timHandle.Init.ClockDivision = 0;
   __GPIOH_CLK_ENABLE();
   __TIM12_CLK_ENABLE();


   GPIO_InitTypeDef gpio;
   gpio.Pin = GPIO_PIN_6;
   gpio.Alternate = GPIO_AF9_TIM12;
   gpio.Mode = GPIO_MODE_AF_PP;
   gpio.Pull = GPIO_NOPULL;
   gpio.Speed = GPIO_SPEED_FAST;
   HAL_GPIO_Init(GPIOH, &gpio);

   /*status = HAL_TIM_Base_Init(&timHandle);
   status = HAL_TIM_Base_Start(&timHandle);*/


   rims_tim_oc.Pulse = 7500-1;
   rims_tim_oc.OCMode = TIM_OCMODE_PWM2;
   rims_tim_oc.OCFastMode = TIM_OCFAST_ENABLE;
   rims_tim_oc.OCIdleState = TIM_OCMODE_ACTIVE;
   rims_tim_oc.OCPolarity = TIM_OCPOLARITY_LOW;
   /*HAL_TIM_OC_Init(&timHandle);
   HAL_TIM_OC_ConfigChannel(&timHandle, &tim_oc, TIM_CHANNEL_1);
   HAL_TIM_OC_Start(&timHandle, TIM_CHANNEL_1);*/

   HAL_TIM_PWM_Init(&rims_timHandle);
   HAL_TIM_PWM_ConfigChannel(&rims_timHandle, &rims_tim_oc, TIM_CHANNEL_1);
   HAL_TIM_PWM_Start(&rims_timHandle, TIM_CHANNEL_1);

}

void rims_pwm_set(float duty) {
   /*rims_tim_oc.Pulse = 100*duty - 1;
   HAL_TIM_PWM_ConfigChannel(&rims_timHandle, &rims_tim_oc, TIM_CHANNEL_1);
   HAL_TIM_PWM_Start(&rims_timHandle, TIM_CHANNEL_1);*/
   TIM12->CCR1 = 100*duty-1;
}
