/**
 ******************************************************************************
 * @file    Templates/Src/main.c
 * @author  MCD Application Team
 * @version V1.0.3
 * @date    22-April-2016
 * @brief   STM32F7xx HAL API Template project
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include "stm32f7xx_hal_usart.h"
#include "stm32f7xx_hal_uart.h"
#include "stm32f7xx_hal_gpio.h"
#include "stm32f746xx.h"
#include "stm32746g_discovery_lcd.h"

#include <string.h>
#include <stdint.h>
#include "delay.h"
#include "print.h"
#include "drivers/onewire.h"
#include "drivers/ds18b20.h"


/** @addtogroup STM32F7xx_HAL_Examples
 * @{
 */

/** @addtogroup Templates
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);
static void MPU_Config(void);
static void CPU_CACHE_Enable(void);

extern void print_temp(float tmp);
extern void print_maxTemp(float tmp);
extern void print_minTemp(float tmp);
extern void print_mashTemp(float tmp);
extern void print_rimsTemp(float tmp);

extern void test_onewire(void);
extern void pwm_init(void);
extern void pwm_set(float);

extern void rims_pwm_init(void);
extern void rims_pwm_set(float);

uint8_t aTxBuffer[] = "MicroBrew control system\r\nUART initialized, initializing display.\r\n";
char dbg_buf[256];
char *str;
/* Private functions ---------------------------------------------------------*/

static int pump = 0;
static float rimsPwm = 0;
static float tTarget = 0;
static float KpRims = 15;
static float KpMash = 0;
static float KiRims = 0.0001;
static float boilerPwm = 0;
static int mashIn = 1;

void rimsControlPwm(float tMash, float tRims) {

  float eRims;
  float eMash;
  static float eSum = 0;
  static float previousTarget = 0;

  if (tTarget != previousTarget) {
    //eSum = 0;
  }

  if (mashIn == 1) {
    if (tMash < tTarget) {
      rimsPwm = 100;
    } else {
      rimsPwm = 0;
    }
  } else {
    eRims = tTarget - tRims;
    eMash = tTarget - tMash;
    eSum = eSum + eRims;
    rimsPwm = (eSum * KiRims) + (eRims * KpRims) + (eMash * KpMash);

    if (rimsPwm > 100) {
      rimsPwm = 100;
    } else if (rimsPwm < 0) {
      rimsPwm = 0;
    }
  }
  rims_pwm_set(rimsPwm);
}

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void)
{
   int ow[2];
   volatile static int status;
   float maxtemp = 0;
   float mintemp = 100;

   GPIO_InitTypeDef  GPIO_InitStruct;

   /* This project template calls firstly two functions in order to configure MPU feature
     and to enable the CPU Cache, respectively MPU_Config() and CPU_CACHE_Enable().
     These functions are provided as template implementation that User may integrate
     in his application, to enhance the performance in case of use of AXI interface
     with several masters. */
   /* Configure the MPU attributes as Write Through */
   MPU_Config();

   /* Enable the CPU Cache */
   CPU_CACHE_Enable();

   /* STM32F7xx HAL library initialization:
       - Configure the Flash ART accelerator on ITCM interface
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Low Level Initialization
    */
   HAL_Init();

   /* Configure the System clock to
  delay_init();have a frequency of 216 MHz */
   SystemClock_Config();
   pwm_init();
   rims_pwm_init();
   init_uart();
   init_display();
   delay_init();
   //delay_us(1000000);

   /* Add your application code here
    */
   print(aTxBuffer);

   // Temp sensors G6 D2, G7 D5
   // Boiler PWM, D3, GPIOB4 Tim3ch1
   // RIMS PWM, D6, GPIOH6
   // Pump D5, GPIOG7

   ow[0] = onewire_init(GPIOG, GPIO_PIN_6);
   ow[1] = onewire_init(GPIOG, GPIO_PIN_7);
   print("resetting onewire\r\n");
   status = onewire_reset(ow[0]);
   status = onewire_reset(ow[1]);
   print("done\r\n");
   ds18b20_setResolution(ow[0], resolution_12bits);
   ds18b20_setResolution(ow[1], resolution_12bits);

   /* Initialize GPIO pin for Pump*/
   __GPIOG_CLK_ENABLE();
   GPIO_InitStruct.Pin = GPIO_PIN_0;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Pull = GPIO_PULLUP;
   GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
   GPIO_InitStruct.Alternate = 0;
   HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

   (void)status;
   while (1)
   {
      volatile float temp_float;
      static float temp;
      static int wait = 0;


      static float mashTarget = 63;
      static float rimsTarget = 65;


      ds18b20_startConversion(ow[0]);
      ds18b20_startConversion(ow[1]);
      delay_us(1000000);
      temp_float = ds18b20_readTemp(ow[1])/16.0;
      print_mashTemp(temp_float);
      temp = ds18b20_readTemp(ow[0])/16.0;
      print_rimsTemp(temp);

      HAL_GPIO_WritePin(GPIOI, GPIO_PIN_0, pump);




      /* RIMS control loop */
      rimsControlPwm(temp_float, temp);//float tMash, float rRims
      /*if ((temp_float < mashTarget) || (temp < mashTarget)) {
        rims_pwm_set(rimsPwm);
      } else {
        rims_pwm_set(0);
      }*/



      //rims_pwm_set(rimsPwm);
      pwm_set(boilerPwm);

      /*if (temp != E_NOT_PRESENT) {
         if (wait == 0) {
            temp_float = temp/16.0;
            if (temp_float > maxtemp) {
               maxtemp = temp_float;
            }
            if (temp_float < mintemp) {
               mintemp = temp_float;
            }
            print_temp(temp_float);
            print_maxTemp(maxtemp);
            print_minTemp(mintemp);
            if (temp_float > 10) {
               HAL_GPIO_WritePin(GPIOI, GPIO_PIN_1, GPIO_PIN_SET);
            } else if (temp_float < 8) {
               HAL_GPIO_WritePin(GPIOI, GPIO_PIN_1, GPIO_PIN_RESET);
            }
         } else {
            wait = 0;
         }
      } else {
         HAL_GPIO_WritePin(GPIOI, GPIO_PIN_1, GPIO_PIN_RESET);
         print_temp(-99.99);
         wait = 1;
      }*/
   }

}


/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 216000000
 *            HCLK(Hz)                       = 216000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 25000000
 *            PLL_M                          = 25
 *            PLL_N                          = 432
 *            PLL_P                          = 2
 *            PLL_Q                          = 9
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 7
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void)
{
   RCC_ClkInitTypeDef RCC_ClkInitStruct;
   RCC_OscInitTypeDef RCC_OscInitStruct;

   /* Enable HSE Oscillator and activate PLL with HSE as source */
   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
   RCC_OscInitStruct.HSEState = RCC_HSE_ON;
   RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
   RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
   RCC_OscInitStruct.PLL.PLLM = 25;
   RCC_OscInitStruct.PLL.PLLN = 432;
   RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
   RCC_OscInitStruct.PLL.PLLQ = 9;
   if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
   {
      Error_Handler();
   }

   /* activate the OverDrive to reach the 216 Mhz Frequency */
   if(HAL_PWREx_EnableOverDrive() != HAL_OK)
   {
      Error_Handler();
   }

   /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
   RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
   RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
   if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
   {
      Error_Handler();
   }
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void)
{
   /* User may add here some code to deal with this error */
   while(1)
   {
   }
}

/**
 * @brief  Configure the MPU attributes as Write Through for SRAM1/2.
 * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
 *         The Region Size is 256KB, it is related to SRAM1 and SRAM2  memory size.
 * @param  None
 * @retval None
 */
static void MPU_Config(void)
{
   MPU_Region_InitTypeDef MPU_InitStruct;

   /* Disable the MPU */
   HAL_MPU_Disable();

   /* Configure the MPU attributes as WT for SRAM */
   MPU_InitStruct.Enable = MPU_REGION_ENABLE;
   MPU_InitStruct.BaseAddress = 0x20010000;
   MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
   MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
   MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
   MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
   MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
   MPU_InitStruct.Number = MPU_REGION_NUMBER0;
   MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
   MPU_InitStruct.SubRegionDisable = 0x00;
   MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

   HAL_MPU_ConfigRegion(&MPU_InitStruct);

   /* Enable the MPU */
   HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
 * @brief  CPU L1-Cache enable.
 * @param  None
 * @retval None
 */
static void CPU_CACHE_Enable(void)
{
   /* Enable I-Cache */
   SCB_EnableICache();

   /* Enable D-Cache */
   SCB_EnableDCache();
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
   /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

   /* Infinite loop */
   while (1)
   {
   }
}
#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
