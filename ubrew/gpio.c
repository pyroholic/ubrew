#include "gpio.h"

void GPIO_setDirection(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin, uint32_t mode) {
   uint8_t pin_position;
   /* Get pin number */
   for(pin_position=0; pin_position<GPIO_NUMBER;pin_position++) {
      /* Find active pin */
      if ((1u << pin_position) & GPIO_Pin) {
         uint32_t temp = GPIOx->MODER;
         temp &= ~(GPIO_MODE << (2 * pin_position));
         GPIOx->MODER |= temp | ((mode&GPIO_MODE) << (2 * pin_position));
         break;
      }
   }
}
