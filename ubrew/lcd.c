#include <string.h>
#include "stm32746g_discovery_lcd.h"
#include "print.h"
#include "delay.h"
#include <stdint.h>
#include <stdlib.h>

extern const struct {
   unsigned int     width;
   unsigned int     height;
   unsigned int     bytes_per_pixel; /* 2:RGB16, 3:RGB, 4:RGBA */
   unsigned char    pixel_data[480 * 272 * 2 + 1];
} gimp_image;

void init_display(void) {
   char dbg_buf[256];
   uint32_t xSize, ySize;
   uint32_t ret;
   BSP_LCD_Init();
   BSP_LCD_DisplayOff();
   print("LCD initialized\r\n");
     ySize = BSP_LCD_GetYSize();
     //dbg_buf
     ret = sprintf(dbg_buf, "Y-size: %i\r\n", ySize);
     print(dbg_buf);
     xSize = BSP_LCD_GetXSize();
     ret = sprintf(dbg_buf, "X-size: %i\r\n", xSize);
     print(dbg_buf);
     print("Initializing layers\r\n");
     BSP_LCD_LayerRgb565Init(0, LCD_FB_START_ADDRESS);

     memcpy((void *)LCD_FB_START_ADDRESS, gimp_image.pixel_data, (xSize*ySize*2));
     BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS+(BSP_LCD_GetXSize()*BSP_LCD_GetYSize()*4));

     print("Clearing screen");
     /* Infinite loop */
     /* Enable the LCD */


       /* Select the LCD Background Layer  */
       BSP_LCD_SelectLayer(0);

       /* Clear the Background Layer */
       //BSP_LCD_Clear(LCD_COLOR_BLACK);

       /* Select the LCD Foreground Layer  */
       BSP_LCD_SelectLayer(1);

       /* Clear the Foreground Layer */
       BSP_LCD_Clear(LCD_COLOR_WHITE);

       BSP_LCD_SetTransparency(0, 0xff);
       BSP_LCD_SetTransparency(1, 0xff);
       BSP_LCD_SetColorKeying(1, 0xffffffff);
       BSP_LCD_SetTextColor(LCD_COLOR_RED);
       delay_us(20000);
       BSP_LCD_DisplayOn();
}

void print_temp(float tmp) {
   char str[64];
   sprintf(str, "  Temp: %3i.%03iC", (int)tmp, abs((int)((tmp*1000))%1000));
   BSP_LCD_DisplayStringAtLine(1, str);
}

void print_maxTemp(float tmp) {
   char str[64];
   sprintf(str, "  Max:  %3i.%03iC", (int)tmp, abs((int)((tmp*1000))%1000));
   BSP_LCD_DisplayStringAtLine(2, str);
}

void print_minTemp(float tmp) {
   char str[64];
   sprintf(str, "  Min:  %3i.%03iC", (int)tmp, abs((int)((tmp*1000))%1000));
   BSP_LCD_DisplayStringAtLine(3, str);
}

void print_mashTemp(float tmp) {
   char str[64];
   sprintf(str, "  Mash: %3i.%03iC", (int)tmp, abs((int)((tmp*1000))%1000));
   BSP_LCD_DisplayStringAtLine(3, str);
}

void print_rimsTemp(float tmp) {
   char str[64];
   sprintf(str, "  RIMS: %3i.%03iC", (int)tmp, abs((int)((tmp*1000))%1000));
   BSP_LCD_DisplayStringAtLine(2, str);
}
