#include <string.h>
#include "stm32f4xx_hal.h"

UART_HandleTypeDef UartHandle;

void init_uart(void) {

   UartHandle.Instance        = UART4;

   UartHandle.Init.BaudRate   = 115200;
   UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
   UartHandle.Init.StopBits   = UART_STOPBITS_1;
   UartHandle.Init.Parity     = UART_PARITY_NONE;
   UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
   UartHandle.Init.Mode       = UART_MODE_TX_RX;
   UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
   //UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

   HAL_UART_DeInit(&UartHandle);
   HAL_UART_Init(&UartHandle);
}

void print(char * str) {
   int length = strlen(str);
   HAL_UART_Transmit(&UartHandle, str, length, 5000);
}
